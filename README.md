# jupyterhub-docker

## Requirements

- [Ubuntu OS](https://ubuntu.com/), it shoud be above Ubuntu 18.04.

- [Git](https://git-scm.com/), read [instructions](https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-20-04) to install Git on Ubuntu.

- [Docker](https://www.docker.com/), read [instructions](https://docs.docker.com/engine/install/ubuntu/) to install Docker on Ubuntu.

## Usage

### Clone this repo to local

- Open a terminal.

- Run the following command lines in the terminal.

```bash
$ git clone https://gitlab.com/dinhvn/jupyterhub-docker.git
$ cd jupyterhub-docker
```

### Create `jovyan/docker-stacks-foundation:latest` image

- Change line 6 in [docker-stacks-foundation/Dockerfile](docker-stacks-foundation/Dockerfile) to your needs.

- **Note:** Choose your root container carefully, which should be from trustworthy and reliable sources, for example: `nvidia/cuda`.

```dockerfile
ARG ROOT_CONTAINER=nvidia/cuda:11.3.0-cudnn8-devel-ubuntu20.04
```

- Run the following command lines in the terminal.

```bash
$ cd docker-stacks-foundation
$ docker build . < Dockerfile -t jovyan/docker-stacks-foundation:latest
```

- Wait for building `jovyan/docker-stacks-foundation:latest` image finish. Result in the terminal should be similar to:

```bash
=> [12/12] WORKDIR /home/jovyan
=> exporting to image
=> => exporting layers
=> => writing image sha256:02959380e4147e4756e1685cef683dac29df5834d1dd5c61360d8186fbf011ec
=> => naming to docker.io/jupyter/docker-stacks-foundation:latest
```

### Create your jupyter notebook gpu docker image

- Run the following command lines in the terminal.

```bash
$ cd base-notebook-gpu
$ docker build . < Dockerfile -t your-tag
```

- Wait for building your jupyter notebook gpu docker image finish.
